//提交图片
    public function ajaxOrgImg()
    {
        //上传图片
        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize  = 10145728 ;// 设置附件上传大小
        $upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->rootPath  =  './Public/'; // 设置附件上传根目录
        $upload->savePath  =  'org/'; // 设置附件上传根目录
        $upload->autoSub = true;
        $upload->subName = array('date','Ymd');
        $upload->saveName = array('uniqid', array('', true));  //保持上传文件名
        $info=$upload->upload();
        if(!$info) {// 上传错误提示错误信息
//            $this->error($upload->getError());
        }else{// 上传成功 获取上传文件信息
            foreach($info as $file){
                $image = new \Think\Image();
                $open = './Public/'.$file['savepath'].$file['savename'];
                $image->open($open);
                $image->thumb(300, 300)->save($open);
                $imgurl  = '/Public/'.$file['savepath'].$file['savename'];
                $data['uid'] = S('uinfo');
                $imgNum=M('img')->where($data)->select();
                $imgNum=count($imgNum);
//                print_r($imgNum);
                if($imgNum>8){
                    $res['res'] = 2;
                    echo json_encode($res);
                    exit;
                }
                $data['img_url'] = $imgurl;
                $img = M('img')->add($data);
                if($img){
                    $res['res'] = 1;
                    $res['id'] = $img;
                    $res['img'] = $data['img_url'];
                    echo json_encode($res);
                    exit;
                }else{
                    $res['res'] = 0;
                    $res['word'] = '上传失败';
                    echo json_encode($res);
                    exit;
                }
            }
        }

    }